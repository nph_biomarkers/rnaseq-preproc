#!/bin/bash
#SBATCH -t 120:00:00
#SBATCH -n 4
#SBATCH -N 16
#SBATCH -J stringtie3
#SBATCH --mem=32GB
#SBATCH --mail-type=ALL
#SBATCH --mail-user=zachary_levin@brown.edu
#SBATCH -o /gpfs/data/afleisc2/NPH/NPH_cohort_1/logs/stringtie3_%a_%j.out
#SBATCH -e /gpfs/data/afleisc2/NPH/NPH_cohort_1/logs/stringtie3_%a_%j.err

module load stringtie/1.3.4d

for prefix in $(ls /gpfs/data/afleisc2/NPH/NPH_cohort_1/bam_alignments/*.srtd.bam)
do
echo ${prefix}
file_name=${prefix##*/}
echo ${file_name}
file=${file_name%%.*}


stringtie ${prefix} -p 16 -e -B -G /gpfs/data/afleisc2/NPH/NPH_cohort_1/stringtie_output/stringtie_merged_ref.gtf -o /gpfs/data/afleisc2/NPH/NPH_cohort_1/stringtie_output/${file}_GRCh38_merged.gtf

done



