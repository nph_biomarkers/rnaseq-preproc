#!/bin/bash
#SBATCH -t 01:00:00
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -J trimming_qc
#SBATCH --array=0-10
#SBATCH --mem=1GB
#SBATCH --mail-type=ALL
#SBATCH --mail-user=zachary_levin@brown.edu
#SBATCH -o /gpfs/data/afleisc2/NPH/NPH_cohort_1/logs/trimming_qc_%a_%j.out
#SBATCH -e /gpfs/data/afleisc2/NPH/NPH_cohort_1/logs/trimming_qc_%a_%j.err


file_names=(AF-CSF-01 AF-CSF-02 AF-CSF-03 AF-CSF-04 AF-CSF-05 AF-CSF-06 AF-CSF-07 AF-CSF-08 AF-CSF-09 AF-CSF-10 AF-CSF-11)

echo ${file_names[$SLURM_ARRAY_TASK_ID]}
