#!/bin/bash
#SBATCH -t 72:00:00
#SBATCH -n 4
#SBATCH -N 4
#SBATCH -J sam_view_sort
#SBATCH --mem=24GB
#SBATCH --mail-type=ALL
#SBATCH --mail-user=zachary_levin@brown.edu
#SBATCH -o /gpfs/data/afleisc2/NPH/NPH_cohort_1/logs/sam_view_sort_%a_%j.out
#SBATCH -e /gpfs/data/afleisc2/NPH/NPH_cohort_1/logs/sam_view_sort_%a_%j.err

module load samtools/1.9

for prefix in $(ls /gpfs/data/afleisc2/NPH/NPH_cohort_1/alignments_2/*.sam)
do
echo ${prefix}
file_name=${prefix##*/}
echo ${file_name}

samtools view -S -b ${prefix} > /gpfs/data/afleisc2/NPH/NPH_cohort_1/bam_alignments/${file_name}.unsrtd.bam

echo "Done samtools view with ${prefix}"

done

for file in $(ls /gpfs/data/afleisc2/NPH/NPH_cohort_1/bam_alignments/*.unsrtd.bam)
do
echo ${file}

samtools sort ${file} -o ${file%.unsrtd.bam}.srtd.bam

echo "Done samtools sort with ${file}"
done

echo "Samtools view and sort complete!"
