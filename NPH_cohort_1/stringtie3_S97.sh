#!/bin/bash
#SBATCH -t 02:00:00
#SBATCH -n 4
#SBATCH -N 4
#SBATCH -J stringtie3
#SBATCH --mem=32GB
#SBATCH --mail-type=ALL
#SBATCH --mail-user=zachary_levin@brown.edu
#SBATCH -o /gpfs/data/afleisc2/NPH/NPH_cohort_1/logs/stringtie3_S97_%a_%j.out
#SBATCH -e /gpfs/data/afleisc2/NPH/NPH_cohort_1/logs/stringtie3_S97_%a_%j.err

module load stringtie/1.3.4d

stringtie /gpfs/data/afleisc2/NPH/NPH_cohort_1/bam_alignments/AF-CSF-11T-FC01_S97.sam.srtd.bam -p 16 -e -B -G /gpfs/data/afleisc2/NPH/NPH_cohort_1/stringtie_output/stringtie_merged_ref.gtf -o /gpfs/data/afleisc2/NPH/NPH_cohort_1/stringtie_output/AF-CSF-11T-FC01_S97_GRCh38_merged.gtf

echo "Done"


