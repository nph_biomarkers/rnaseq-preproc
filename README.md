Project Overview
================

This repo contains the command line shell scripts used for all of the mapping, alignment,and QC of the RNAseq data from the UChicago sequencing facility.

Due to slight formatting differences between batches of data from UChicago, each cohort has a separate set of scripts (each a directory in the repo). They perform analagous functions and differ only slightly in file names and the like.

These scripts are written to run on Oscar as all of the RNA processing occurs on Oscar.

The directory structure is roughly the following: (All Fleischmann Lab members have access to the NPH files in the lab's data folder)
- data
    - NPH
        - NPH_cohort_1
            - raw_fastq
            - trimmed_fastq
            - bam_alignments
            - alignment_summary
            - stringtie_output
            - scripts
            - logs
        - NPH_cohort_2 (comparable to NPH_cohort_1)
        - NPH_combined_cohorts
            - scripts
            - logs
            - merged_gtfs_folders


The steps of the workflow are the following: (Same info available on shared Drive under Zach>RNAseq Workflow>NPH RNAseq Pipeline (Hisat2))

Reference Genome:
GrCh38 release 104 from ensembl
Gtf and primary sequence file from https://ftp.ensembl.org/pub/release-104/

Step 0:
Build HISAT2 index with --ss and --exon using the annotation gtf file

Step 1:
Remove adapter sequences with Trimgalore
Default parameters for paired sequencing
FastQC on the trimmed fastq files
Script = ‘trimgalore_fastqc.sh’

Step 2:
HISAT Alignment using GrCh38 as reference
--dta flag!
Script = ‘hisat.sh’

Step 3:
Samtools view and sort (convert .sam files to srtd.bam files)
Default samtools sorting parameters
Script = ‘sam_view_sort.sh’

Step 4:
Stringtie 1
Script = ‘stringtie1.sh’

Step 5:
Stringtie 2 (merge)
Input file = input_gtf_files.txt
Output = stringtie_merged_ref.gtf
Script = ‘stringtie_merge.sh’

Step 6:
Stringtie 3 (final mapping)
Uses the merged gtf file and outputs GRCh38_merged.gtf plus Ballgown tables in folder for each sample
Script = ‘stringtie_3.sh’

Step 7:
Transfer the data (~80GB from Oscar to local computer)
Import the ballgown files to R using tximport and summarize the transcript expression to gene-level expression following documentation

