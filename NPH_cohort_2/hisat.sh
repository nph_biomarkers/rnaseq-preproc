#!/bin/bash
#SBATCH -t 168:00:00
#SBATCH -n 4
#SBATCH -N 4
#SBATCH -J hisat_alignment2
#SBATCH --mem=32GB
#SBATCH --mail-type=ALL
#SBATCH --mail-user=zachary_levin@brown.edu
#SBATCH -o /gpfs/data/afleisc2/NPH/NPH_cohort_2/logs/hisat_alignment2_%a_%j.out
#SBATCH -e /gpfs/data/afleisc2/NPH/NPH_cohort_2/logs/hisat_alignment2_%a_%j.err

module load hisat2/2.1.0

for prefix in $(ls /gpfs/data/afleisc2/NPH/NPH_cohort_2/trimmed_fastq/*.fq.gz | sed -r 's/_R[12]_001_val_[12][.]fq.gz//' | uniq)
do
echo ${prefix}
file_name=${prefix##*/}
echo ${file_name}

hisat2 -p 8 --dta -x /gpfs/data/afleisc2/NPH/genome_ref/GRCh38.rel_104/genome_tran -1 ${prefix}_R1_001_val_1.fq.gz -2 ${prefix}_R2_001_val_2.fq.gz -S /gpfs/data/afleisc2/NPH/NPH_cohort_2/alignments_2/${file_name}.sam --summary-file /gpfs/data/afleisc2/NPH/NPH_cohort_2/alignment_summary/${file_name}.txt

done
