#!/bin/bash
#SBATCH -t 24:00:00
#SBATCH -n 4
#SBATCH -N 4
#SBATCH -J sam_view_sort_S35
#SBATCH --mem=24GB
#SBATCH --mail-type=ALL
#SBATCH --mail-user=zachary_levin@brown.edu
#SBATCH -o /gpfs/data/afleisc2/NPH/NPH_cohort_2/logs/sam_view_sort_S35_%a_%j.out
#SBATCH -e /gpfs/data/afleisc2/NPH/NPH_cohort_2/logs/sam_view_sort_S35_%a_%j.err

module load samtools/1.9

samtools view -S -b /gpfs/data/afleisc2/NPH/NPH_cohort_2/alignments/AF-CSF-EV-048T_S35.sam > /gpfs/data/afleisc2/NPH/NPH_cohort_2/alignments/AF-CSF-EV-048T_S35.unsrtd.bam

echo "Done samtools view with ${prefix}"

samtools sort /gpfs/data/afleisc2/NPH/NPH_cohort_2/alignments/AF-CSF-EV-048T_S35.unsrtd.bam -o /gpfs/data/afleisc2/NPH/NPH_cohort_2/alignments/AF-CSF-EV-048T_S35.unsrtd.srtd.bam

echo "Done samtools sort with ${file}"

echo "Samtools view and sort S35 complete!"
