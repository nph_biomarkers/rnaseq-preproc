#!/bin/bash
#SBATCH -t 48:00:00
#SBATCH -n 2
#SBATCH -N 2
#SBATCH -J trimming_qc_FC03
#SBATCH --mem=48GB
#SBATCH --mail-type=ALL
#SBATCH --mail-user=zachary_levin@brown.edu
#SBATCH -o /gpfs/data/afleisc2/NPH/NPH_cohort_2/logs/trimming_qc_FC03_%a_%j.out
#SBATCH -e /gpfs/data/afleisc2/NPH/NPH_cohort_2/logs/trimming_qc_FC03_%a_%j.err

module load trimgalore/0.5.0 fastqc/0.11.5 cutadapt/1.14


for prefix in $(ls /gpfs/data/afleisc2/NPH/NPH_cohort_2/raw_fastq/FC03/FastQ/*.fastq.gz | sed -r 's/_R[12]_001[.]fastq.gz//' | uniq):
do
echo ${prefix}

trim_galore --illumina --fastqc -o /gpfs/data/afleisc2/NPH/NPH_cohort_2/trimmed_fastq/ --paired ${prefix}_R1_001.fastq.gz ${prefix}_R2_001.fastq.gz

done
  
echo "Done with FC03"
