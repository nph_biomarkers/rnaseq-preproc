#!/bin/bash
#SBATCH -t 48:00:00
#SBATCH -n 4
#SBATCH -N 4
#SBATCH -J hisat_alignment_S35
#SBATCH --mem=32GB
#SBATCH --mail-type=ALL
#SBATCH --mail-user=zachary_levin@brown.edu
#SBATCH -o /gpfs/data/afleisc2/NPH/NPH_cohort_2/logs/hisat_alignment_S35_%a_%j.out
#SBATCH -e /gpfs/data/afleisc2/NPH/NPH_cohort_2/logs/hisat_alignment_S35_%a_%j.err

module load hisat2/2.1.0

hisat2 -p 8 -x /gpfs/data/afleisc2/NPH/hisat_stringtie/genome_ref/grch37/genome -1 /gpfs/data/afleisc2/NPH/NPH_cohort_2/trimmed_fastq/AF-CSF-EV-048T_S35_R1_001_val_1.fq.gz -2 /gpfs/data/afleisc2/NPH/NPH_cohort_2/trimmed_fastq/AF-CSF-EV-048T_S35_R2_001_val_2.fq.gz -S /gpfs/data/afleisc2/NPH/NPH_cohort_2/alignments/AF-CSF-EV-048T_S35.sam

echo "Done with FC01 S35"

hisat2 -p 8 -x /gpfs/data/afleisc2/NPH/hisat_stringtie/genome_ref/grch37/genome -1 /gpfs/data/afleisc2/NPH/NPH_cohort_2/trimmed_fastq/AF-CSF-EV-048T-FC02_S35_R1_001_val_1.fq.gz -2 /gpfs/data/afleisc2/NPH/NPH_cohort_2/trimmed_fastq/AF-CSF-EV-048T-FC02_S35_R2_001_val_2.fq.gz -S /gpfs/data/afleisc2/NPH/NPH_cohort_2/alignments/AF-CSF-EV-048T-FC02_S35.sam

echo "Done with FC02 S35"

hisat2 -p 8 -x /gpfs/data/afleisc2/NPH/hisat_stringtie/genome_ref/grch37/genome -1 /gpfs/data/afleisc2/NPH/NPH_cohort_2/trimmed_fastq/AF-CSF-EV-048T-FC03_S35_R1_001_val_1.fq.gz -2 /gpfs/data/afleisc2/NPH/NPH_cohort_2/trimmed_fastq/AF-CSF-EV-048T-FC03_S35_R2_001_val_2.fq.gz -S /gpfs/data/afleisc2/NPH/NPH_cohort_2/alignments/AF-CSF-EV-048T-FC03_S35.sam

echo "Done with FC03 S35"

echo "All complete"
