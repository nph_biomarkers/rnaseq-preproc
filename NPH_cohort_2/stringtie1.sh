#!/bin/bash
#SBATCH -t 168:00:00
#SBATCH -n 6
#SBATCH -N 6
#SBATCH -J stringtie1
#SBATCH --mem=20GB
#SBATCH --mail-type=ALL
#SBATCH --mail-user=zachary_levin@brown.edu
#SBATCH -o /gpfs/data/afleisc2/NPH/NPH_cohort_2/logs/stringtie1_%a_%j.out
#SBATCH -e /gpfs/data/afleisc2/NPH/NPH_cohort_2/logs/stringtie1_%a_%j.err

module load stringtie/1.3.4d

for prefix in $(ls /gpfs/data/afleisc2/NPH/NPH_cohort_2/bam_alignments/*.srtd.bam)
do
echo ${prefix}
file_name=${prefix##*/}
echo ${file_name}
file=${file_name%%.*}

stringtie ${prefix} -p 16 -G /gpfs/data/afleisc2/NPH/genome_ref/genome.gtf -o /gpfs/data/afleisc2/NPH/NPH_cohort_2/stringtie_output/${file}_GRCh38.gtf

done
