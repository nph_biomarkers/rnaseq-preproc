#!/bin/bash
#SBATCH -t 48:00:00
#SBATCH -n 4
#SBATCH -N 4
#SBATCH -J stringtie_merge
#SBATCH --mem=24GB
#SBATCH --mail-type=ALL
#SBATCH --mail-user=zachary_levin@brown.edu
#SBATCH -o /gpfs/data/afleisc2/NPH/NPH_cohort_2/logs/stringtie_merge_%a_%j.out
#SBATCH -e /gpfs/data/afleisc2/NPH/NPH_cohort_2/logs/stringtie_merge_%a_%j.err

module load stringtie/1.3.4d

stringtie --merge -p 4 -G /gpfs/data/afleisc2/NPH/genome_ref/genome.gtf -o /gpfs/data/afleisc2/NPH/NPH_cohort_2/stringtie_output/stringtie_merged_ref.gtf /gpfs/data/afleisc2/NPH/NPH_cohort_2/scripts/input_gtf_paths.txt




