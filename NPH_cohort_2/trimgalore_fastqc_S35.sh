#!/bin/bash
#SBATCH -t 24:00:00
#SBATCH -n 4
#SBATCH -N 4
#SBATCH -J trimming_qc_S35
#SBATCH --mem=24GB
#SBATCH --mail-type=ALL
#SBATCH --mail-user=zachary_levin@brown.edu
#SBATCH -o /gpfs/data/afleisc2/NPH/NPH_cohort_2/logs/trimming_qc_S35_%a_%j.out
#SBATCH -e /gpfs/data/afleisc2/NPH/NPH_cohort_2/logs/trimming_qc_S35_%a_%j.err

module load trimgalore/0.5.0 fastqc/0.11.5 cutadapt/1.14


trim_galore --illumina --fastqc -o /gpfs/data/afleisc2/NPH/NPH_cohort_2/trimmed_fastq/ --paired /gpfs/data/afleisc2/NPH/NPH_cohort_2/raw_fastq/FC01/FastQ/AF-CSF-EV-048T_S35_R1_001.fastq.gz /gpfs/data/afleisc2/NPH/NPH_cohort_2/raw_fastq/FC01/FastQ/AF-CSF-EV-048T_S35_R2_001.fastq.gz
  
echo "Done with FC01 S35"

trim_galore --illumina --fastqc -o /gpfs/data/afleisc2/NPH/NPH_cohort_2/trimmed_fastq/ --paired /gpfs/data/afleisc2/NPH/NPH_cohort_2/raw_fastq/FC02/FastQ/AF-CSF-EV-048T-FC02_S35_R1_001.fastq.gz /gpfs/data/afleisc2/NPH/NPH_cohort_2/raw_fastq/FC02/FastQ/AF-CSF-EV-048T-FC02_S35_R2_001.fastq.gz
  
echo "Done with FC02 S35"

trim_galore --illumina --fastqc -o /gpfs/data/afleisc2/NPH/NPH_cohort_2/trimmed_fastq/ --paired /gpfs/data/afleisc2/NPH/NPH_cohort_2/raw_fastq/FC03/FastQ/AF-CSF-EV-048T-FC03_S35_R1_001.fastq.gz /gpfs/data/afleisc2/NPH/NPH_cohort_2/raw_fastq/FC03/FastQ/AF-CSF-EV-048T-FC03_S35_R2_001.fastq.gz
  
echo "Done with FC03 S35"
