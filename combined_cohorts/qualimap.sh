#!/bin/bash
#SBATCH -t 72:00:00
#SBATCH -n 6
#SBATCH -N 6
#SBATCH -J qualimap2
#SBATCH --mem=32GB
#SBATCH --mail-type=ALL
#SBATCH --mail-user=zachary_levin@brown.edu
#SBATCH -o /gpfs/data/afleisc2/NPH/NPH_combined_cohorts/logs/qualimap2_%a_%j.out
#SBATCH -e /gpfs/data/afleisc2/NPH/NPH_combined_cohorts/logs/qualimap2_%a_%j.err

module load qualimap/2.2.1

for prefix in $(ls /gpfs/data/afleisc2/NPH/NPH_cohort_1/bam_alignments/*.srtd.bam)
do
echo ${prefix}
file_name=${prefix##*/}
file=${file_name%.srtd.bam}
echo ${file}

qualimap rnaseq -pe -bam ${prefix} -gtf /gpfs/data/afleisc2/NPH/NPH_combined_cohorts/merged_gtfs/stringtie_all_merged_ref.gtf -outdir /gpfs/data/afleisc2/NPH/NPH_combined_cohorts/qualimap/ --outfile /gpfs/data/afleisc2/NPH/NPH_combined_cohorts/qualimap/${file}.pdf --java-mem-size=32G
 
done

for prefix in $(ls /gpfs/data/afleisc2/NPH/NPH_cohort_2/bam_alignments/*.srtd.bam)
do
echo ${prefix}
file_name=${prefix##*/}
file=${file_name%.srtd.bam}
echo ${file}

qualimap rnaseq -pe -bam ${prefix} -gtf /gpfs/data/afleisc2/NPH/NPH_combined_cohorts/merged_gtfs/stringtie_all_merged_ref.gtf -outdir /gpfs/data/afleisc2/NPH/NPH_combined_cohorts/qualimap/ --outfile /gpfs/data/afleisc2/NPH/NPH_combined_cohorts/qualimap/${file}.pdf --java-mem-size=32G
 
done

#-outdir /gpfs/data/afleisc2/NPH/NPH_combined_cohorts/qualimap/
